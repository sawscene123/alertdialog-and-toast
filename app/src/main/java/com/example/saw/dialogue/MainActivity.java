package com.example.saw.dialogue;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button btnShowDialog = findViewById(R.id.btn_show_dialog);
        Button btnShowListDialog = findViewById(R.id.btn_show_list_dialog);
        Button btnShowCustomViewDialog = findViewById(R.id.btn_show_custom_view_dialog);

//        LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE); // prone null value
        LayoutInflater inflater = LayoutInflater.from(this); // null value safe

        btnShowDialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new AlertDialog.Builder(MainActivity.this)
                        .setCancelable(false)
                        .setTitle("This is title")
                        .setMessage("Do you want to logout?")
                        .setPositiveButton("Logout", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Log.e(MainActivity.class.getSimpleName(), "Positive button clicked");
                                dialog.dismiss();
                            }
                        })
                        .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Log.e(MainActivity.class.getSimpleName(), "negative button clicked");
                                dialog.dismiss();
                            }
                        })
                        .create()
                        .show();
                        /*.setNeutralButton("Neutral", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Log.e(MainActivity.class.getSimpleName(), "Neutral button clicked");
                                dialog.dismiss();
                            }
                            })
                        })*/
            }
        });

        btnShowListDialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String[] genders = new String[]{"Male", "Female", "Others"};

                new AlertDialog.Builder(MainActivity.this)
                        .setTitle("Select Your Gender")
                        .setItems(genders, new DialogInterface.OnClickListener() {//to add list of items
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
//                                switch (which){
//                                    case 0:
//                                        Log.e(MainActivity.class.getSimpleName(), "You are male");
//                                        break;
//                                    case 1:
//                                        Log.e(MainActivity.class.getSimpleName(), "You are female");
//                                        break;
//                                    case 2:
//                                        Log.e(MainActivity.class.getSimpleName(), "You are oher");
//                                        break;
//                                }
                                String gender = genders[which];
                                Log.e(MainActivity.class.getSimpleName(), "your gender is " + gender);
                                dialog.dismiss();
                            }
                        })
                        .create()
                        .show();

            }
        });

        btnShowCustomViewDialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                View view = LayoutInflater.from(MainActivity.this)
                        .inflate(R.layout.layout_dialog_input, null, false);
                final EditText etUsername = view.findViewById(R.id.et_username);
                final AlertDialog dialog = new AlertDialog.Builder(MainActivity.this)
                        .setView(view)
                        .create();
                view.findViewById(R.id.btn_login).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Toast.makeText(MainActivity.this, "You've logged in successfully", Toast.LENGTH_SHORT).show();
                        Log.e(MainActivity.class.getSimpleName(), "Username: " + etUsername.getText());
                        dialog.dismiss();
                    }
                });
                view.findViewById(R.id.btn_cancel).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });
                dialog.show();
            }
        });
    }
}
